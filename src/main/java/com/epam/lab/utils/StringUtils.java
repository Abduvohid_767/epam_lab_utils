package com.epam.lab.utils;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author Abduvohid Isroilov
 * @date 2021-06-14
 * @time 14:20
 */
public class StringUtils {

    public static boolean isPositiveNumber(String str) {
        int number = -1;
        if (org.apache.commons.lang3.StringUtils.isNumeric(str)) {
            number = NumberUtils.toInt(str);
        }
        return number >= 0;
    }

}
